﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        private ILazyLoader _lazyLoader;
        private List<Preference> _preferences = new List<Preference>();

        public Customer() { }
        public Customer(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        [MaxLength(100)]
        public string FirstName { get; set; }
        
        [MaxLength(100)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(100)]
        public string Email { get; set; }

        public List<Preference> Preferences {
            get => _lazyLoader.Load(this, ref _preferences); 
            set => _preferences = value; 
        } 

        public List<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();

    }
}