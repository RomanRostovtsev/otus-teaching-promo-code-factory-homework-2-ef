﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {

        [MaxLength(100)]
        public string Name { get; set; }

        public List<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();

        public List<Customer> Customers { get; set; } = new List<Customer>();
    }
}