﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository, IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promocodeRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns>Массив по всем клиентам</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {

            var customers = await _customerRepository.GetAllAsync();
            IEnumerable<CustomerShortResponse> result = customers.Select(c => new CustomerShortResponse()
            {
                Id = c.Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            });
            return new ActionResult<IEnumerable<CustomerShortResponse>>(result);
        }

        /// <summary>
        /// Получить данные клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <returns>Данные клиента</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();
            CustomerResponse response = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName

            };
            IEnumerable<PromoCode> promocodes = await _promoCodeRepository.GetAllAsync();
            promocodes = promocodes.Where(pc => pc.CustomerId.Equals(id));
            response.PromoCodes = new List<PromoCodeShortResponse>();
            foreach(PromoCode promocode in promocodes)
            {
                response.PromoCodes.Add(new PromoCodeShortResponse()
                {
                    Id = promocode.Id,
                    Code = promocode.Code,
                    PartnerName = promocode.PartnerName,
                    ServiceInfo = promocode.ServiceInfo,
                    BeginDate = promocode.BeginDate.ToShortDateString(),
                    EndDate = promocode.EndDate.ToShortDateString()
                });
            }
            response.Preferences = new List<PreferenceResponse>();
            foreach(Preference preference in customer.Preferences)
            {
                response.Preferences.Add(new PreferenceResponse() { Id = preference.Id, Name = preference.Name });
            }
            return new ActionResult<CustomerResponse>(response);
        }

        /// <summary>
        /// Добавить нового клиента и информацию о его предпочтениях
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            Customer customer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            if (request.PreferenceIds != null)
            {
                foreach(Guid preferenceId in request.PreferenceIds)
                {
                    Preference p = await _preferenceRepository.GetByIdAsync(preferenceId);
                    if (p == null) return BadRequest();
                    customer.Preferences.Add(p);
                }
            }
            await _customerRepository.AddAsync(customer);
            return Ok();
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();
            IEnumerable<Preference> allPreferences = null;
            if (request.PreferenceIds != null)
            {
                allPreferences = await _preferenceRepository.GetAllAsync();
                foreach (var preferenceId in request.PreferenceIds)
                {
                    if (allPreferences.FirstOrDefault(p => p.Id.Equals(preferenceId)) == null) return BadRequest();
                }
            }
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            if (request.PreferenceIds == null)
            {
                customer.Preferences.Clear();
            }
            else { 
                for (var i = customer.Preferences.Count - 1; i >= 0; i--)
                {
                    if (!request.PreferenceIds.Contains(customer.Preferences[i].Id))
                    {
                        customer.Preferences.RemoveAt(i);
                    }
                }
                foreach (var preferenceId in request.PreferenceIds)
                {
                    if (customer.Preferences.Find(p => p.Id.Equals(preferenceId)) == null)
                    {
                        customer.Preferences.Add(allPreferences.First(p => p.Id.Equals(preferenceId)));
                    }
                }
            }
            await _customerRepository.UpdateAsync();
            return Ok();
        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промокодами
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                await _customerRepository.DeleteByIdAsync(id);
            }
            catch (ArgumentException) {
                return NotFound();
            }
            return Ok();
        }
    }
}