﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, 
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<Customer> customersRepository)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customersRepository = customersRepository;
        }


        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            IEnumerable<PromoCode> promocodes = await _promocodeRepository.GetAllAsync();
            return promocodes.Select(code => new PromoCodeShortResponse()
            {
                Id = code.Id,
                PartnerName= code.PartnerName,
                Code= code.Code,
                BeginDate = code.BeginDate.ToShortDateString(),
                EndDate= code.EndDate.ToShortDateString()
            });
        }

        /// <summary>
        /// Создать промокод и выдать его клиентУ с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preference = preferences.FirstOrDefault(pref => String.Equals(pref.Name, request.Preference));
            if (preference == null) return BadRequest();
            var customers = await _customersRepository.GetAllAsync();
            var customer = customers.FirstOrDefault(c => c.Preferences.Contains(preference));
            DateTime now = DateTime.Now;
            PromoCode promoCode = new PromoCode()
            {
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                BeginDate = now,
                EndDate = now.Add(PromoCode.DefaultInterval),
                Preference = preference,
                Customer = customer
            };
            await _promocodeRepository.AddAsync(promoCode);
            return Ok();
        }
    }
}