﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
         : IRepository<T>
        where T : BaseEntity
    {

        private PromoCodeDatabase _database;
        public EfRepository(PromoCodeDatabase database) {
            _database = database;
        }


        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _database.Set<T>().ToListAsync();
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return _database.Set<T>().FirstOrDefaultAsync(entity => entity.Id.Equals(id));            
        }
        
        public async Task AddAsync(T item)
        {
            await _database.Set<T>().AddAsync(item);
            await _database.SaveChangesAsync();
        }

        public Task UpdateAsync()
        {
            return _database.SaveChangesAsync();
        }

        public async Task  DeleteByIdAsync(Guid id) {
            T item = await GetByIdAsync(id);
            if (item == null) throw new ArgumentException(nameof(id));
            _database.Set<T>().Remove(item);
            await _database.SaveChangesAsync();
        }

    }
}
