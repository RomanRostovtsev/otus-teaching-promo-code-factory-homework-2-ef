﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeDatabase : DbContext
    {
        private static bool _initializedWithData = false;
        public PromoCodeDatabase(bool initializeWithData = true): base() 
        {
            if (initializeWithData)
            {
                EnsureInitializedWithData();
            }
        }

        private void EnsureInitializedWithData()
        {
            lock (this)
            {
                if (!_initializedWithData)
                {
                    Database.EnsureDeleted();
                    Database.Migrate();
                    Roles.AddRange(FakeDataFactory.Roles);
                    Employees.AddRange(FakeDataFactory.Employees);
                    var preferences = FakeDataFactory.Preferences;
                    Preferences.AddRange(preferences);
                    var customers = FakeDataFactory.Customers;
                    foreach(var customer in customers)
                    {
                        for(var i = 0; i < customer.Preferences.Count; ++i)
                        {
                            var trackedPreference = preferences.First(pref=>pref.Id.Equals(customer.Preferences[i].Id));
                            customer.Preferences[i] = trackedPreference;
                        }
                        
                    }
                    Customers.AddRange(customers);
                    SaveChanges();
                    _initializedWithData = true;
                }
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlite("DataSource=PromoCodes.db");
        }


        public virtual DbSet<Role> Roles { get; set; }

        public virtual DbSet<Employee> Employees { get; set; }

        public virtual DbSet<Preference> Preferences { get; set; }

        public virtual DbSet<Customer> Customers { get; set; }

        public virtual DbSet<PromoCode> PromoCodes { get; set; }


    }
}
