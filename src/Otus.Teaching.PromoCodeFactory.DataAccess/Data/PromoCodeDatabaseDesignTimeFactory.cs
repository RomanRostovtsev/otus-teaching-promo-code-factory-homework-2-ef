﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeDatabaseDesignTimeFactory : IDesignTimeDbContextFactory<PromoCodeDatabase>
    {
        public PromoCodeDatabase CreateDbContext(string[] args)
        {
            return new PromoCodeDatabase(false);
        }
    }
}
